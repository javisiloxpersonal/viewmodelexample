package com.cice.modules

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.util.Log
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class MainViewModel : ViewModel() {

    var weather: MutableLiveData<WeatherData> = MutableLiveData()

    companion object {
        fun create(activity: MainActivity): MainViewModel {
            return ViewModelProviders.of(activity).get(MainViewModel::class.java)
        }
    }

    init {
        doAsync {
            Log.d("CICE", "VOY A LLAMAR")
            var url = "https://api.apixu.com/v1/current.json?key=14955594788d4b3cb3e142828192402&q=London"
            var response = URL(url).readText()
            Log.d("CICE", "HE LLAMADO")
            uiThread {
                Log.d("CICE", "response")
                var weatherResponse = Gson().fromJson(response, WeatherData::class.java)
                //Parser
                Log.d("CICE", "objeto parseado")
                weather.value = weatherResponse
                Log.d("CICE", "actualizado life data")

            }
        }
    }
}